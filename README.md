# LiveJOSM
LiveJOSM is [Debian][] operating system with preinstalled [JOSM][] and some
useful [JOSM plugins][] that can boot without installation to a hard drive.
LiveJOSM is based on [DebianLive][].

Some ready to download images are available at http://livejosm.mapathon.cz/.

**Note:** Username and password is: `user` / `live`.

**Note:** UEFI Secure Boot is under development. It has to be switched off for
now to boot successfully.

[Debian]: https://www.debian.org/
[JOSM]: https://josm.openstreetmap.de/
[JOSM plugins]: https://josm.openstreetmap.de/wiki/Plugins
[DebianLive]: https://wiki.debian.org/DebianLive/

# Contribute
## Code
For quick orientation see the [changelog][].

Please, think about [The seven rules of a great Git commit message][] when
making commit. The project use [OneFlow][] branching model with the `master`
branch as the branch where the development happens.

## License
This project is developed under [GNU GPLv3 license][].

## Build
You need `live-build` package to be able to build LiveJOSM image. Then, the
steps to build it are the following:
```
lb clean
bash download.sh
lb config
lb build
```

## Localization
Modify `auto/config` file to enable localization. For example:

Czech
```
LOC="locales=cs_CZ.UTF-8"
KB="keyboard-layouts=cz"
TZ="timezone=Europe/Prague"
```

French
```
LOC="locales=fr_FR.UTF-8"
KB="keyboard-layouts=fr"
TZ="timezone=Europe/Paris"
```

German
```
LOC="de_DE.UTF-8"
KB="keyboard-layouts=de"
TZ="timezone=Europe/Berlin"
```

Slovak
```
LOC="locales=sk_SK.UTF-8"
KB="keyboard-layouts=sk"
TZ="timezone=Europe/Bratislava"
```

[changelog]: ./CHANGELOG.md
[The seven rules of a great Git commit message]: https://chris.beams.io/posts/git-commit/
[OneFlow]: http://endoflineblog.com/oneflow-a-git-branching-model-and-workflow
[GNU GPLv3 license]: ./LICENSE
