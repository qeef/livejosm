#!/bin/bash

# Download JOSM
wget https://josm.openstreetmap.de/josm-tested.jar
mv josm-tested.jar ./config/includes.chroot/usr/share/java/

# Download JOSM plugins
wget https://svn.openstreetmap.org/applications/editors/josm/dist/buildings_tools.jar
wget https://qeef.gitlab.io/mapathoner/mapathoner.jar
wget https://svn.openstreetmap.org/applications/editors/josm/dist/terracer.jar
wget https://github.com/JOSM/todo/releases/download/v30306/todo.jar
wget https://svn.openstreetmap.org/applications/editors/josm/dist/FastDraw.jar

mv buildings_tools.jar mapathoner.jar terracer.jar todo.jar FastDraw.jar \
	./config/includes.chroot/etc/skel/.local/share/JOSM/plugins/

# Download Missing Maps wallpaper
wget http://www.missingmaps.org/assets/graphics/layout/Intro.svg
mv Intro.svg ./config/includes.chroot/etc/skel/

# Download JOSM icon
wget https://josm.openstreetmap.de/logo.png
mv logo.png ./config/includes.chroot/usr/share/pixmaps/josm.png

# Update preferences to current version
CV=$(wget josm.openstreetmap.de -q -O - | sed -n 's/\(^[0-9]\{5,\}\).*/\1/p' | head -1)
sed -i "s/'14460'/'$CV'/g" ./config/includes.chroot/etc/skel/.config/JOSM/preferences.xml
