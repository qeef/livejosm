# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog][] and this project adheres to
[Semantic Versioning][].

[Keep a Changelog]: http://keepachangelog.com/
[Semantic Versioning]: http://semver.org/

## [Unreleased][]

## [0.2.0][] - 2019-03-09
### Added
- Timezone to config script.
- Missing Maps wallpaper.

### Changed
- Prevent messagebox asking for plugin updates. Current version of each plugin
  is downloaded when building the image by `download.sh` script.
- Disable screensaver and screen locking.

### Fixed
- JOSM version mismatch in preferences.

## 0.1.0 - 2018-12-01
### Added
- Changelog, license, readme.
- Non-free firmware support for ethernet/wifi cards.
- Xfce GUI.
- Localization.
- JOSM.

[Unreleased]: https://gitlab.com/qeef/livejosm/compare/v0.2.0...master
[0.2.0]: https://gitlab.com/qeef/livejosm/compare/v0.1.0...v0.2.0
